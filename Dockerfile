FROM alpine:edge

ARG VERSION

ENV ADDITIONAL_MODULES="ws" \
    TZ="UTC" \
    DATA_FOLDER="/data" \
    UID=1000 \
    GID=1000

WORKDIR /app
RUN set -e && \
    apk add --no-cache --virtual .build-deps git python3 build-base ca-certificates && \
    apk add --no-cache --virtual .run-deps nodejs npm graphicsmagick tzdata su-exec && \
    ln -sf /usr/bin/python3 /usr/bin/python && \
    npm_config_user=root npm_config_unsafe_perm="true" npm install -g full-icu && \
    npm_config_user=root npm_config_unsafe_perm="true" npm install sqlite3 n8n@${VERSION}  && \
    npm_config_user=root npm_config_unsafe_perm="true" npm uninstall -g full-icu && \
    npm_config_user=root npm_config_unsafe_perm="true" npm cache clean --force && \
    apk del .build-deps && \
    rm -rf /usr/bin/python -rf /tmp/* /var/cache/apk/*

COPY entrypoint.sh /app/entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["/app/node_modules/n8n/bin/n8n"]
